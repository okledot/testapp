﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;


namespace TestApp_search
{
    public class SearchEngine
    {
        public string msg = "Выполнить поиск по следующим критериям?:";
        //public string dir = folderBrowserDialog1.SelectedPath;


    }
}
        /* Проба поисковика
            public  ()
            {
                InitializeComponent();

                var dir = @"f:\";
                var fileMask = "*.*";

                //корневая директория
                var root = new Node { FullPath = dir };

                //поток
                var thread = new Thread(() => BuildTree(root, "*.*")) { IsBackground = true };
                thread.Start();

                //дерево
                var ft = new FastTree() { Parent = this, Dock = DockStyle.Fill, ShowIcons = true, ShowRootNode = true };
                ft.NodeIconNeeded += ft_NodeIconNeeded;

                //обновляем дерево, пока работает поток
                Application.Idle += delegate
                {
                    if (thread.IsAlive)
                    {
                        ft.Build(root);
                        Text = "Found files: " + found;
                    }
                };
            }

            void ft_NodeIconNeeded(object sender, ImageNodeEventArgs e)
            {
                if (!(e.Node as Node).IsFile)
                    e.Result = Properties.Resources.folder;
            }

            private int found;

            //посторение дерева
            private void BuildTree(Node root, string fileMask)
            {
                var toProcess = new Stack<Node>();
                toProcess.Push(root);

                while (toProcess.Count > 0)
                {
                    var node = toProcess.Pop();
                    try
                    {
                        foreach (var dir in Directory.GetDirectories(node.FullPath))
                        {
                            var n = new Node { FullPath = dir };
                            node.Add(n);
                            toProcess.Push(n);
                        }

                        foreach (var file in Directory.GetFiles(node.FullPath, fileMask))
                        {
                            var n = new Node { FullPath = file, IsFile = true };
                            node.Add(n);
                            found++;
                        }
                    }
                    catch (UnauthorizedAccessException)
                    {
                    }
                }
            }

            private void Form1_Load(object sender, EventArgs e)
            {

            }
        }

        //Элемент файловой системы
        public class Node : IEnumerable<Node>
        {
            private List<Node> nodes;
            //Файл?
            public bool IsFile { get; set; }
            //Полный путь
            public string FullPath { get; set; }
            //Имя
            public string Name { get { return Path.GetFileName(FullPath); } }

            // Имеет хоть один файл у себя или у потомков ?
            public bool HasFile
            {
                get
                {
                    return IsFile || Nodes.Any(n => n.HasFile);
                }
            }

            public Node()
            {
                nodes = new List<Node>();
            }

            public override string ToString()
            {
                return string.IsNullOrEmpty(Name) ? FullPath : Name;
            }

            //добавление нода
            public void Add(Node node)
            {
                nodes.Add(node);
            }

            IEnumerable<Node> Nodes
            {
                get
                {
                    for (int i = 0; i < nodes.Count; i++)
                        yield return nodes[i];
                }
            }

            public IEnumerator<Node> GetEnumerator()
            {
                return Nodes.Where(n => n.HasFile).GetEnumerator();
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
    */
