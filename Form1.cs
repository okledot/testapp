﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace TestApp_search
{
    public partial class Form1 : Form
    {
        TreeNode mainnode = new TreeNode();
        Boolean stopSearch = false;
        string dir, name, inside;
        string sdir, sname, sins;
        int maxprogval = 10000;
        const string CONFIGFILENAME = "config.cfg";
        public Form1()
        {
            InitializeComponent();

            loadConfig();

            //System.IO.StreamReader streamReader = new System.IO.StreamReader(@"config.cfg");
            //string str;
            //    str = streamReader.ReadLine();
            //        string[] splitDir = str.Split('=');
            //        dir = splitDir[1];
            //    str = streamReader.ReadLine();
            //        string[] splitName = str.Split('=');
            //        name = splitName[1];
            //    str = streamReader.ReadLine();
            //        string[] splitInside = str.Split('=');
            //        inside = splitInside[1];
            //streamReader.Close();

            this.textBox3.Text = dir;
            this.textBox4.Text = name;
            this.textBox5.Text = inside;
            //MessageBox.Show(dir + "\n" + name + "\n" + inside);
        }

        private void loadConfig() {
            System.IO.StreamReader streamReader;
            string str;
            string[] confData = new string[2];


            if (System.IO.File.Exists(CONFIGFILENAME))
            {
                streamReader = new System.IO.StreamReader(CONFIGFILENAME);
                while ((str = streamReader.ReadLine()) != null)
                {
                    confData = str.Split('=');
                    switch (confData[0])
                    {
                        case "dir":
                            dir = confData[1];
                            break;
                        case "name":
                            name = confData[1];
                            break;
                        case "inside":
                            inside = confData[1];
                            break;
                    }
                }
                streamReader.Close();
            }
            else
            {
                MessageBox.Show("Будет создан файл: " + CONFIGFILENAME +
                    "\n" + "Он нужен для сохранения заданных параметров", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                System.IO.StreamWriter textFile = new System.IO.StreamWriter(CONFIGFILENAME);
                textFile.WriteLine("dir=");
                textFile.WriteLine("name=");
                textFile.WriteLine("inside=");
                textFile.Close();
            }
        }


        private void saveConfig(string param, string val) {
            string oldResult = "";
            string str;
            string[] confData = new string[2];
            System.IO.StreamReader streamReader = new System.IO.StreamReader(CONFIGFILENAME);
            while ((str = streamReader.ReadLine()) != null)
            {
                confData = str.Split('=');
                if (confData[0] == param) {
                    oldResult = str;
                    streamReader.Close();
                    break;
                }
            }
            File.WriteAllText(CONFIGFILENAME, 
                File.ReadAllText(CONFIGFILENAME).Replace(oldResult, param + "=" + val));
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_Click_1(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                this.textBox4.Enabled = true;
                this.textBox4.ReadOnly = false;
            }
            else
            {
                this.textBox4.Enabled = false;
                this.textBox4.ReadOnly = true;
            }
        }

        private void checkBox2_Click_1(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                this.textBox5.Enabled = true;
                this.textBox5.ReadOnly = false;
            }
            else
            {
                this.textBox5.Enabled = false;
                this.textBox5.ReadOnly = true;            }
        }

        private void DirectoryChoose_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            {
                folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyComputer;

            }
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                
                this.textBox3.Text = folderBrowserDialog1.SelectedPath;

        }


        private void search_Click(object sender, EventArgs e)
        {
            var msgdir = textBox3.Text;
            var msgname = "";
            var msginside = "";
            if (this.checkBox1.Checked == true)
            {
                msgname = textBox4.Text;
            }
            else
            {
                msgname = "";
            }
            if (this.checkBox2.Checked == true)
            {
                msginside = textBox5.Text;
            }
            else
            {
                msginside = "";
            }
            MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
            DialogResult result = MessageBox.Show("Выполнить поиск по следующим критериям ?: " +
                                                    "\n" + "Директория поиска:  " + msgdir +
                                                    "\n" + "Имя файла:  " + msgname +
                                                    "\n" + "Наполнение файла:  " + msginside,
                                                    "Поиск", buttons, MessageBoxIcon.Question);

            if (result == DialogResult.OK)
            {
                if (!backgroundWorker1.IsBusy)
                {
                    saveConfig("dir", msgdir);
                    saveConfig("name", msgname);
                    saveConfig("inside", msginside);
                    sdir = msgdir;
                    sname = msgname;
                    sins = msginside;

                    treeView.Nodes.Clear();
                    stopSearch = false;
                    progBar.Maximum = maxprogval;
                    cancelBtn.Enabled = true;
                    backgroundWorker1.RunWorkerAsync();
                }
                else {
                    MessageBox.Show("Вы уже выполняете поиск!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //treeView.Nodes.Add(getDirTree(msgdir, msgname,msginside));
                
                
                //System.IO.StreamWriter textFile = new System.IO.StreamWriter(CONFIGFILENAME);
                //textFile.WriteLine("dir=" + msgdir);
                //textFile.WriteLine("name=" + msgname);
                //textFile.WriteLine("inside=" + msginside);
                //textFile.Close();
            }


        }

        #region "Фоновый поиск"

        private TreeNode getDirTree(String root, string fname="",string ins="" ) {
            if (!stopSearch)
            {
                backgroundWorker1.ReportProgress(20);
                TreeNode nd = new TreeNode(Path.GetFileName(root));
                try
                {
                    String[] dirList = Directory.GetDirectories(root);
                    String[] fileList = Directory.GetFiles(root);
                    foreach (string dir in dirList)
                    {
                        nd.Nodes.Add(getDirTree(dir, fname, ins));
                    }




                    foreach (string file in fileList)
                    {
                        if (fname != "")
                        {

                            if (ins != "")
                            {
                                if (File.ReadAllText(file).IndexOf(ins) >= 0 && Path.GetFileName(file).Contains(fname))
                                {
                                    nd.Nodes.Add(Path.GetFileName(file));
                                    continue;
                                }
                            }
                            else
                            {
                                if (Path.GetFileName(file).Contains(fname))
                                {
                                    nd.Nodes.Add(Path.GetFileName(file));
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            if (ins == "")
                            {
                                nd.Nodes.Add(Path.GetFileName(file));
                                continue;
                            }
                            else
                            {
                                if (File.ReadAllText(file).IndexOf(ins) >= 0)
                                {
                                    nd.Nodes.Add(Path.GetFileName(file));
                                    continue;
                                }
                            }
                        }
                    }
                    //foreach (string file in fileList) {
                    //    if (fname != "")
                    //    {
                    //        if (Path.GetFileName(file).Contains(fname))
                    //        {
                    //            nd.Nodes.Add(Path.GetFileName(file));
                    //            continue;
                    //        }
                    //    }
                    //    else {
                    //        if (ins == "")
                    //        {
                    //            nd.Nodes.Add(Path.GetFileName(file));
                    //            continue;
                    //        }
                    //    }

                    //    if (ins != "") {
                    //        string s = File.ReadAllText(file);
                    //        int i = s.IndexOf(ins);
                    //        if (File.ReadAllText(file).IndexOf(ins) >= 0) {
                    //            nd.Nodes.Add(Path.GetFileName(file));
                    //            continue;
                    //        }
                    //    }
                    //}
                }
                catch (Exception e) { }
                return nd;
            }
            else
            { return null; }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            mainnode =getDirTree(sdir, sname, sins);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            treeView.Nodes.Add(mainnode);
            progBar.Value = 0;
            cancelBtn.Enabled = false;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
            progBar.Increment(1);
            if (progBar.Value == maxprogval) { progBar.Value = 0; }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }

        #endregion


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        #region "Обработка изменения полей"
        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            saveConfig("dir", textBox3.Text);
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            saveConfig("name", textBox4.Text);
        }

        

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            saveConfig("inside", textBox5.Text);
        }
        #endregion

        

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy) {
                stopSearch = true;
                backgroundWorker1.ReportProgress(100);
                backgroundWorker1.CancelAsync();
                backgroundWorker1.Dispose();
            }

            cancelBtn.Enabled = false;
        }

       

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Пример:" +
                            "\n" + "Один файл: file.txt или .txt" +
                            "\n" + "Несколько форматов (через пробел): .txt .cfg .html", "Справка" +
                            "\n" + "Наполнение", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
